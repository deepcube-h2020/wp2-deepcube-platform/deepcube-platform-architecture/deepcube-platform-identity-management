# Deployment DeepCube Platform Identity Management


Deploy the DeepCube Platform Identity Management component. It acts as a central User Identity and Authorization service used accross the platform.

## Getting started

1. `source vault.fish`
2. Create secrets with `ansible-vault create secret_vars/all.yml  --vault-password-file "./.vault_password"`
3. `ansible-playbook -i hosts/all.yml deploy.yml --vault-password-file "./.vault_password"`

## Configure platform services to use IAM

### Portainer

todo

### Grafana

todo

### Caddy

todo

### HopsWorks

todo

### Jupyter